package devops

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class DevopsApplication {

	static void main(String[] args) {
		SpringApplication.run(DevopsApplication, args)
	}

}
